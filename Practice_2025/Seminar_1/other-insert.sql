CREATE TEMP TABLE employees (
    id SERIAL PRIMARY KEY,
    name VARCHAR(100) NOT NULL,
    age INT,
    address VARCHAR(100),
    salary DECIMAL(10, 2)
);

INSERT INTO employees (name, age, address, salary) VALUES
('John Doe', 30, '123 Elm St', 50000),
('Jane Smith', NULL, '456 Oak St', NULL),
('Alice Johnson', 25, NULL, 45000),
('Bob Brown', 40, '789 Pine St', 60000);


-- 1. Найдите сотрудников с неопределённой зарплатой.
-- 2. Выберите сотрудников с известной зарплатой.
-- 3. Выведите сотрудников по убыванию зарплаты.
-- 4. Замените неопределённую зарплату на 0.


CREATE TEMP TABLE measurements (
    id SERIAL PRIMARY KEY,
    value FLOAT
);

INSERT INTO measurements (value) VALUES
(1.5),
(2.3),
('NaN'),
(4.7);

-- 5. Выведите все измерения.
-- 6. Выведите все измерения без NaN значений.
-- 7. Выведите сумму всех измерений.
-- 8. Выведите сумму всех измерений исключая NaN значения.

CREATE TEMP TABLE events (
    id SERIAL PRIMARY KEY,
    event_name VARCHAR(100) NOT NULL,
    event_date DATE NOT NULL
);

INSERT INTO events (event_name, event_date) VALUES
('New Year', '2025-01-01'),
('Christmas', '2025-01-07'),
('Valentine\'s Day', '2025-02-14'),
('Праздник Весны и Труда', '2025-05-01'),
('День Победы', '2025-05-09'),
('День народного единства', '2025-11-04');

-- 9. Найдите все события первой половины 2025-го года.
-- 10. Выведите количество событий в мае.
-- 11. Для каждого месяца 2025-го года выведите количество событий.
-- 12. Получите список праздников, которые выпадают на пятницу.
-- 13. Получените следующий праздник после текущей даты.
