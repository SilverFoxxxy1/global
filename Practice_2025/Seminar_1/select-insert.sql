DROP SCHEMA IF EXISTS sem1 CASCADE;
CREATE SCHEMA sem1;
-- DROP SCHEMA sem1 CASCADE;

CREATE TABLE sem1.suppliers (
    supplier_id SERIAL PRIMARY KEY,
    name VARCHAR(100),
    country VARCHAR(50)
);

INSERT INTO sem1.suppliers (name, country) VALUES
('Global Tech Supplies', 'USA'),
('Eco Products Inc.', 'Russia'),
('Quality Goods Co.', 'USA'),
('Mexican Imports LLC', 'Mexico'),
('Northern Lights Trading', 'Canada'),
('Sunshine Distributors', 'Russia'),
('Green Earth Supplies', 'USA');

CREATE TABLE sem1.customers (
    customer_id SERIAL PRIMARY KEY,
    name VARCHAR(100),
    country VARCHAR(50)
);

INSERT INTO sem1.customers (name, country) VALUES
('Alice Johnson', 'Russia'),
('Bob Smith', 'Canada'),
('Charlie Brown', 'USA'),
('Global Tech Supplies', 'UK'),
('Eco Products Inc.', 'Russia'),
('David Williams', 'USA'),
('Emily Davis', 'Mexico');

CREATE TABLE sem1.products (
    product_id SERIAL PRIMARY KEY,
    name VARCHAR(100),
    price DECIMAL(10, 2),
    supplier_id INT,
    FOREIGN KEY (supplier_id) REFERENCES sem1.suppliers(supplier_id)
);

INSERT INTO sem1.products (name, price, supplier_id) VALUES
('Apple iPhone 14', 999.99, 1),
('Samsung Galaxy S22', 899.99, 1),
('Sony WH-1000XM4 Headphones', 349.99, 2),
('Dell XPS 13 Laptop', 1299.99, 3),
('Bose QuietComfort Earbuds', 279.99, 3);

-- 1. Подсчитать количество продуктов у каждого поставщика.
-- 2. Подсчитать количество клиентов по странам.
-- 3. Определить, сколько продуктов стоит больше $500 и меньше или равно $500 (для каждого продукта определите категорию и разбейте на две категории).
-- 4. Найти поставщиков, у которых более одного продукта.
-- 5. Найти страны с более чем одним клиентом.
-- 6. Определить скидку на продукты в зависимости от их цены.
-- Сначала для каждого продукта определите категорию по стоимости.
-- Для дорогих продуктов (от 1000$) скидка 20%. Для продуктов со средней стоимостью (от 500$ до 1000$) скидка 10%.
-- 7. Определить статус клиентов в зависимости от страны (Local для Russia / International для других стран).

