-- 1. Подсчитать количество продуктов у каждого поставщика.
SELECT supplier_id, COUNT(*) AS product_count
FROM products
GROUP BY supplier_id;

-- 2. Подсчитать количество клиентов по странам.
SELECT country, COUNT(*) AS customer_count
FROM customers
GROUP BY country;

-- 3. Определить, сколько продуктов стоит больше $500 и меньше или равно $500 (для каждого продукта определите категорию и разбейте на две категории).
SELECT 
    CASE 
        WHEN price > 500 THEN 'Above $500'
        ELSE 'At or Below $500' 
    END AS price_category,
    COUNT(*) AS product_count
FROM products
GROUP BY price_category;

-- 4. Найти поставщиков, у которых более одного продукта.
SELECT supplier_id, COUNT(*) AS product_count
FROM products
GROUP BY supplier_id
HAVING COUNT(*) > 1;

-- 5. Найти страны с более чем одним клиентом.
SELECT country, COUNT(*) AS customer_count
FROM customers
GROUP BY country
HAVING COUNT(*) > 1;

-- 6. Определить скидку на продукты в зависимости от их цены.
-- Сначала для каждого продукта определите категорию по стоимости.
-- Для дорогих продуктов (от 1000$) скидка 20%. Для продуктов со средней стоимостью (от 500$ до 1000$) скидка 10%.
SELECT name, price,
    CASE 
        WHEN price > 1000 THEN '20%'
        WHEN price BETWEEN 500 AND 1000 THEN '10%'
        ELSE 'No Discount'
    END AS discount_rate
FROM products;

-- 7. Определить статус клиентов в зависимости от страны (Local для Russia / International для других стран).
SELECT name, CASE WHEN (country = 'Russia') THEN 'Local' ELSE 'International' END AS customer_type
FROM sem1.customers;
